let express = require("express");
const app = express();
const port = 3000;

app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.get("/", (req, res)=>{
    res.send("Hello World")
})

app.get("/hello",(req,res) =>{
    res.send("Hello from the /hello endpoint")
})

app.post("/hello", (req,res) => {
    console.log(req.body.firstName);
    console.log(req.body.lastName);
    res.send(`Hello there ${req.body.firstName} ${req.body.lastName}`)
})

// ACTIVITY

app.get("/home", (req,res) => {
    res.send("Welcome to the home page.")
})

app.get("/users", (req,res) => {
    res.send([{
        "username": "johndoe",
        "password": "johndoe1234"
    }])
})

app.delete("/delete-user", (req,res) => {
    console.log(req.body.username);
    res.send(`User ${req.body.username} has been deleted.`)
})

app.listen(port, () => console.log(`Server is running at port ${port}`));